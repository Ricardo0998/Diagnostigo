/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

import java.util.Date;

/**
 *
 * @author rodri
 */
public class DatosAdministrador {
    
    private static Date fecha;
    private String nombreAdmin; 

    public DatosAdministrador(String nombreAdmin) {
        this.nombreAdmin = nombreAdmin;
    }
    
    public static Date getInstance(){
        if (fecha == null) {
            fecha = new Date();
        }
        return fecha;
    }

    public String getNombreAdmin() {
        return nombreAdmin;
    }
    
}
